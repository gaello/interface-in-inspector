# Assigning Interfaces in Unity Inspector

This repository contain RequireInterface attribute for Unity Editor. You can check how it was created!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2020/01/27/assigning-interface-in-unity-inspector/

Enjoy!

---

# How to use it?

This repository contains an example of how you can make workaround for assigning interfaces in Unity Inspector.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/interface-in-inspector/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
